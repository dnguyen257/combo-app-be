var mongoose = require('mongoose');

function isObjectId(value) {
    try {
        const { ObjectId } = mongoose.Types;
        const asString = value.toString(); // value is either ObjectId or string or anything
        const asObjectId = new ObjectId(asString);
        const asStringifiedObjectId = asObjectId.toString();
        return asString === asStringifiedObjectId;
    } catch (error) {
        return false;
    }
}
/**
 *  random date
 * @param {Date} start start date
 * @param {Date} end start date
 * @returns {Date}
 */
const  randomDate = (start, end) => () => {
    var diff =  end.getTime() - start.getTime();
    var new_diff = diff * Math.random();
    var date = new Date(start.getTime() + new_diff);
    return date;
}

/**
 *  random number in range
 * @param {number} start start number
 * @param {number} end end number
 * @returns {number}
 */
const randomNumberInRange = (start, end) => {
    return Math.floor(Math.random() * (end - start)) + start
}

exports.isObjectId = isObjectId
exports.randomDate = randomDate
