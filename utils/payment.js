const axios = require('axios')
// const addToWallet = async (user_id, value, order_id, callback) => {
//     var options = {
//         method: 'POST',
//         uri: 'https://waza-payment.herokuapp.com/add_to_membership',
//         body: {
//             user_id, value, order_id
//         },
//         json: true
//     }
//     return axios.post(options, (error, response) => {
//         if (error) {
//             callback(error, undefined)
//         }
//         else {
//             callback(undefined, response.body.response)
//         }
//     })
// }

const addToWallet = async (user_id, value, order_id) => axios({
    method: 'POST',
    url: 'https://waza-payment.herokuapp.com/add_to_membership',
    responseType: "json",
    data: {
        user_id, value, order_id
    },
    timeout: 30000,
    headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    },
}).then(_ => 200)
.catch(_ => 400)

module.exports = { addToWallet }