const request = require('request')
const getpassengers = (token,callback) => {
    var options = {
        method:'GET',
        url: 'https://app-wazapassenger.herokuapp.com/api/passengers',
        headers: {
            'Content-Type': 'application/json',
            'authorization': token
        },
        json:true
    }
    request(options,(error,{body}) => {
        if(body.error) {
            callback(body.error,undefined)
        } else if (body.length === 0) {
            callback('No users!', undefined)
        } else {
            callback(undefined,body)
        }
    })
}

module.exports = getpassengers