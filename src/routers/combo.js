const Combo = require('../models/combo')
const Voucher = require('../models/voucher')
const ComboUser = require('../models/comboUser')
const VoucherUser = require('../models/voucherUser')

const auth = require('../middleware/auth')
const JWT = require('jsonwebtoken')

require('../db/mongoose')
const express = require('express')
const router = new express.Router();
// List combo
// GET /combos?limit=10&page=0
router.get('/combos',async (req,res)=> {
    //parameter:
    // page: page number
    // limit: number of record per page
    let search = {}
    if(req.query.search) {
        const searchString = req.query.search.split('%')
        searchString.forEach((str) => {
            let singleSearch = str.split(':')
            search[singleSearch[0]] = singleSearch[1]
        })
    }
    const pageOptions = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    //sort parameter: sortBy
    //example:sortBy=<property>:desc || asc
    const sort = {}
    if(req.query.sortBy) {
        const parts = req.query.sortBy.split(':')
        sort[parts[0]] = parts[1] ==='desc' ? -1:1
    }
    try {
        //Query a list that parameters include
        //skip:page
        //limit:limit
        //sort: sortBy
        const combos = await Combo.find({...search,isDeleted:false})
        .skip(pageOptions.page*pageOptions.limit)
        .limit(pageOptions.limit)
        .sort(sort);
        //declare a new aray to return to client
        if(combos.length==0) {
            return res.status(404).send()
        }
        res.send(combos)
    } catch(e) {
        res.status(500).send(e)
    }
})
//Get active combos
router.get('/combos/active',async (req,res)=> {
    //parameter:
    // page: page number
    // limit: number of record per page
    // search: search the record want to show
    let search = {}
    if(req.query.search) {
        const searchString = req.query.search.split('%')
        searchString.forEach((str) => {
            let singleSearch = str.split(':')
            search[singleSearch[0]] = singleSearch[1]
        })
    }
    const pageOptions = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    //sort parameter: sortBy
    //example:sortBy=<property>:desc || asc
    const sort = {}
    if(req.query.sortBy) {
        const parts = req.query.sortBy.split(':')
        sort[parts[0]] = parts[1] ==='desc' ? -1:1
    }
    try {
        //Query a list that parameters include
        //skip:page
        //limit:limit
        //sort: sortBy 
        const combos = await Combo.find({...search,state:true,isDeleted:false,
            to_date: { $gt: Date.now()},from_date: {$lt:Date.now()}})
        .skip(pageOptions.page*pageOptions.limit)
        .limit(pageOptions.limit)   
        .sort(sort);
        if(combos.length==0) {
            return res.status(404).send()
        }
        res.send(combos)
    } catch(e) {
        res.status(500).send(e)
    }
})
//Get combo count
router.get('/combos/count', async (req,res)=> {
    try{ 
        const length = await Combo.countDocuments()
        res.send({length})
    } catch(e) {
        res.status(500).send(e)
    }
})
//get combo by ID
router.get('/combos/:id',async (req,res)=> {
    const _id = req.params.id
    try {
        const combo = await Combo.findById(_id)
        const vouchers = []
        for(const voucher of combo.voucher_array) {
            let singleVoucher = await Voucher.findById(voucher.voucher_id)
            const newvoucher = {
                ...singleVoucher._doc,
                count: voucher.count
            }
            vouchers.push(newvoucher)
        }
        if(!combo) {
            return res.status(404).send()
        }
        res.send({...combo._doc,vouchers})
    } catch(e) {
        res.status(500).send(e)
    }
})
//Create a new combo
router.post('/combos/new',async (req,res) => {
    const combo = new Combo(req.body)
    try {
        await combo.save()
        res.status(201).send(combo)
    } catch(e) {
        res.status(400).send(e)
    }
})
//Edit a combo by ID
router.patch('/combos/edit/:id', async(req,res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['combo_name','description','value','state','from_date','to_date','voucher_array','days','policy_id']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    if(!isValidOperation) {
        return res.status(400).send({error: 'Invalid combo updates!'})
    }
    try { 
        const combo = await Combo.findById(req.params.id)
        updates.forEach((update) => combo[update] = req.body[update])
        await combo.save()
        if(!combo) {
            return res.status(404).send()
        }
        res.send(combo)
    } catch(e){
        res.status(400).send(e)
    }
})
//deactive combo
router.patch('/combos/deactive/:id', async(req,res) => {
    try {    
        const combo = await Combo.findById(req.params.id)
        combo.state = false
        await combo.save()
        if(!combo) {
            return res.status(404).send()
        }
        res.send(combo)
    } catch(e){
        res.status(400).send(e)
    }
})
//Active combo
router.patch('/combos/active/:id', async(req,res) => {
    try {    
        const combo = await Combo.findById(req.params.id)
        combo.state = true
        await combo.save()
        if(!combo) {
            return res.status(404).send()
        }
        res.send(combo)
    } catch(e){
        res.status(400).send(e)
    }
})
//Delete a combo by ID
router.patch('/combos/del/:id', async(req,res) => {
    try {
        const combo = await Combo.findById(req.params.id)
        combo.isDeleted = true
        await combo.save()
        if(!combo) {
            return res.status(404).send()
        }
        res.send(combo)
    } catch (e){
        res.status(400).send(e)
    }
})
//Add a voucher to Combo
router.patch('/combos/addvoucher/:id', async(req,res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['voucher_id','count']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    if(!isValidOperation || !req.body.voucher_id ) {
        return res.status(400).send({error: 'Can not add voucher!, following properties are allowed: voucher_id,count'})
    }
    try {
        let flag = "0"
        const combo= await Combo.findById(req.params.id)
        for(const voucher of combo.voucher_array) {
            if(voucher.voucher_id == req.body.voucher_id) {
                flag = "1"
                if(req.body.count) {
                    voucher.count+= parseInt(req.body.count);
                } else {
                    voucher.count+=1;
                }
                break
            }
        }
        if(flag=="0") {
            const newVou = await Voucher.findById(req.body.voucher_id)
            combo.voucher_array.push({voucher_id:newVou.id,voucher_name:newVou.voucher_name, 
                category: newVou.subcategory, discount:newVou.discount, value:newVou.value,
                 count: req.body.count,})
        }
        await combo.save()
        res.status(201).send(combo)
    } catch(e) {
        res.status(400).send(e)
    }
})

module.exports = router