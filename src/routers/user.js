const express = require('express')
const User = require('../models/user')
const router = new express.Router();
const auth = require('../middleware/auth')
require('../db/mongoose')
//List admins
router.get('/admins',async (req,res)=> {
    try {
        const users = await User.find({})
        if(!users) {
            return res.status(404).send()
        }
        res.send(users)
    } catch(e) {
        res.status(500).send(e)
    }
})
//Read profile
router.get('/admins/me',auth,async (req,res)=> {
    res.send(req.user)
})
//Find admin by ID
router.get('/admins/:id',async (req,res)=> {
    const _id = req.params.id
    try {
        const user = await User.findById(_id)
        if(!user) {
            return res.status(404).send()
        }
        res.send(user)
    } catch(e) {
        res.status(500).send(e)
    }
})
//Edit admin
router.patch('/admins/edit/:id', async(req,res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['username','password']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    if(!isValidOperation) {
        return res.status(400).send({error:"Invalid user updates!"})
    }
    try {
        const user = await User.findById(req.params.id)
        updates.forEach((update)=> user[update]=req.body[update])
        await user.save()
        if(!user) {
            return res.status(404).send()
        }
        res.send(user)
    }catch(e){
        res.status(400).send(e)
    }
})
//Edit profile
router.patch('/admins/me', async(req,res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['username','password']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    if(!isValidOperation) {
        return res.status(400).send({error:"Invalid user updates!"})
    }
    try {
        updates.forEach((update)=> req.user[update]=req.body[update])
        await req.user.save()
        res.send(req.user)
    }catch(e){
        res.status(404).send(e)
    }
})
//Delete admin
router.patch('/admins/del/:id', async(req,res) => {
    try {
        const user = await User.findById(req.params.id)
        user.isDeleted = true
        await user.save()
        if(!user) {
            res.status(404).send()
        }
    }catch (e){
        res.status(400).send(e)
    }
})
//Sign up admin
router.post('/admins/signup',async(req,res) => {
    const user = new User(req.body)
    try{
        await user.save()
        const token = await user.generateAuthToken()
        res.status(201).send({user,token})
    } catch(e) {
        res.status(400).send(e)
    }
})
//Login
router.post('/admins/login',async(req,res) => {
    try {
        const user = await User.findByCredentials(req.body.username,req.body.password)
        const token = await user.generateAuthToken()
        res.send({user,token})
    } catch(e) {
        res.status(400).send(e)
    }
})
//Logout
router.post('/admins/logout',auth,async(req,res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})
//Logout all
router.post('/admins/logoutAll',auth,async(req,res) => {
    try {
        req.user.tokens = []
        await req.user.save()
        res.send()
    } catch (e) {
        res.status(500).send()
    }
})
module.exports = router