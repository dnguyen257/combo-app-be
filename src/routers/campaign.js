const express = require('express')
const Campaign = require('../models/campaign')
const VoucherUser = require('../models/voucherUser')
const Voucher = require('../models/voucher')

const passengers = require('../../utils/getpassengers')
const signin = require('../../utils/signin')

require('../db/mongoose')
const router = new express.Router();
const auth = require('../middleware/auth')

//List campaigns
router.get('/campaigns',async (req,res)=> {
    try{ 
        // find a whole list of campaign to response to FE
        const campains = await Campaign.find({})
        //check if list is none to return not found!
        if(campains.length==0) {
            return res.status(404).send()
        }
        res.send(campains)
    } catch(e) { // check if there is any issue on finding document process
        res.status(500).send(e)
    }
})
//Get campaign by ID
router.get('/campaigns/:id',async (req,res)=> {
    //get param
    const _id = req.params.id
    try {
        const campaign = await Campaign.findById(_id)
        if(!campaign) {
            return res.status(404).send()
        }
        //return campaign if founded
        res.send(campaign)
    } catch(e) {
        res.status(500).send(e)
    }
})
//Create campaign
router.post('/campaigns/new',async (req,res) => {
    const campaign = new Campaign(req.body)
    try{
        //save the campaign before give voucher to user
        await campaign.save()
        //try to give vouchers in campaign to user with asynchronous process
        //login first to get list of user in core service
        signin((error,token)=> {
            //check if there is any error before continuing process
            if(error) {
                return console.log(error)
            } else {
                //get list of passengers after having the token of signin API
                passengers(token,async (error,users) => {
                    //check if there is any error before continuing process
                    if(error) {
                        return console.log(error)
                    } else {
                        //declare an array to push list of voucher giving to user in 
                        let uservouArray = []
                        let notificationArray = []
                        // declare a dynamic object to find every 
                        // single voucher from list of objectID that requester giving
                        let dynamicVou = {}
                        for(let vou of campaign.vouchers) {
                            //find voucher by id
                            dynamicVou = await Voucher.findById(vou)
                            for(let user of users) {
                                //check if rank of user is equal to rank of voucher to give it to user
                                if(user["Rank"]==dynamicVou.rank){
                                    //push notification to an array
                                    
                                    //push voucher for user to array using data from campaign, user, dynamicVou
                                    uservouArray.push({owner:user._id,voucher_id:dynamicVou._id,voucher_name:dynamicVou.voucher_name
                                    ,from_date:campaign.from_date, to_date:campaign.to_date, discount:dynamicVou.discount
                                    ,value:dynamicVou.value, service: dynamicVou.subcategory,isUsed:false })

                                }
                            }
                        }
                        //now creating documents in MongoDB from the array that already having list of voucher 
                        VoucherUser.create(uservouArray)
                    }
                })
            }
        })
        res.status(201).send(campaign)
    } catch(e) {
        res.status(400).send(e)
    }
})
//Edit campaign
router.patch('/campaigns/edit/:id', async(req,res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['campaign_name','description','from_date','to_date','vouchers']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    if(!isValidOperation) {
        return res.status(400).send({error: "Invalid campaign updates!"})
    }
    try {
        const campaign = await Campaign.findById(req.params.id)
        updates.forEach((update) => campaign[update]=req.body[update])
        await campaign.save()
        if(!campaign) {
            return res.status(404).send()
        }
        res.send(campaign)
    } catch(e){
        res.status(500).send(e)
    }
})
//Delete campaign
router.patch('/campaigns/del/:id', async(req,res) => {
    try {
        const campaign = await Campaign.findById(req.params.id)
        campaign.isDeleted = true
        await campaign.save()
        if(!campaign) {
            return res.status(404).send()
        }
        res.send(campaign)
    }catch (e){
        res.status(400).send(e)
    }
})

module.exports = router