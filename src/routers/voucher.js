const express = require('express')
const Voucher = require('../models/voucher')
const router = new express.Router();
const auth = require('../middleware/auth')
const VoucherUser = require('../models/voucherUser')
require('../db/mongoose')
//List vouchers
router.get('/vouchers', async (req,res)=> {
    let search = {}
    if(req.query.search) {
        const searchString = req.query.search.split('%')
        searchString.forEach((str) => {
            let singleSearch = str.split(':')
            search[singleSearch[0]] = singleSearch[1]
        })
    }
    const pageOptions = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    const sort = {}
    if(req.query.sortBy) {
        const parts = req.query.sortBy.split(':')
        sort[parts[0]] = parts[1] ==='desc' ? -1:1
    }
    try{ 
        const vouchers = await Voucher.find({...search,isDeleted:false})
        .skip(pageOptions.page*pageOptions.limit)
        .limit(pageOptions.limit)
        .sort(sort)
        if(vouchers.length==0) {
            return res.status(404).send()
        }
        res.send(vouchers)
    } catch(e) {
        res.status(500).send(e)
    }
})
//List active vouchers
router.get('/vouchers/active', async (req,res)=> {
    let search = {}
    if(req.query.search) {
        const searchString = req.query.search.split('%')
        searchString.forEach((str) => {
            let singleSearch = str.split(':')
            search[singleSearch[0]] = singleSearch[1]
        })
    }
    const pageOptions = {
        page: parseInt(req.query.page) || 0,
        limit: parseInt(req.query.limit) || 10
    }
    const sort = {}
    if(req.query.sortBy) {
        const parts = req.query.sortBy.split(':')
        sort[parts[0]] = parts[1] ==='desc' ? -1:1
    }
    try{ 
        const vouchers = await Voucher.find({...search,state:true,isDeleted:false})
        .skip(pageOptions.page*pageOptions.limit)
        .limit(pageOptions.limit)
        .sort(sort)
        if(vouchers.length==0) {
            return res.status(404).send()
        }
        res.send(vouchers)
    } catch(e) {
        res.status(500).send(e)
    }
})  
//Get voucher count
router.get('/vouchers/count', async (req,res)=> {
    try{ 
        const length = await Voucher.countDocuments()
        res.send({length})
    } catch(e) {
        res.status(500).send(e)
    }
})
//Get voucher by ID
router.get('/vouchers/:id',async (req,res)=> {
    const _id = req.params.id
    try {
        const voucher = await Voucher.findById(_id)
        if(!voucher) {
            return res.status(404).send({error:"Not found"})
        }
        res.send(voucher)
    } catch(e) {
        res.status(500).send(e)
    }
})
//Create a new voucher
router.post('/vouchers/new', async (req,res) => {
    const voucher = new Voucher(req.body)
    try{ 
        await voucher.save()
        res.status(201).send(voucher)
    } catch(e) {
        res.status(400).send({message:e.message})
    }
})
//Edit a voucher by ID  
router.patch('/vouchers/edit/:id', async(req,res) => {
    //convert request body to an array to check
    const updates = Object.keys(req.body)
    //check if properties is valid to update
    // foreach property to check
    const allowedUpdates = ['voucher_name','description','discount','value'
    ,'from_date','to_date','state','times_to_use','category','subcategory','rank']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    //if not valid return status 400 with error
    if(!isValidOperation) {
        return res.status(400).send({error: "Invalid voucher updates!"})
    }
    try {
        const voucher = await Voucher.findById(req.params.id)
        if(!voucher) {
            return res.status(404).send({error:"voucher not found"})
        }
        //update voucher with properties
        updates.forEach((update) => voucher[update] = req.body[update])
        await voucher.save()
        res.status(200).send(voucher)
    }catch(e){
        res.status(400).send(e)
    }
})
//deactive voucher
router.patch('/vouchers/deactive/:id', async(req,res) => {
    try {    
        const voucher = await Voucher.findById(req.params.id)
        voucher.state = false
        await voucher.save()
        if(!voucher) {
            return res.status(404).send()
        }
        res.send(voucher)
    } catch(e){
        res.status(400).send(e)
    }
})
//Active voucher
router.patch('/vouchers/active/:id', async(req,res) => {
    try {    
        const voucher = await Voucher.findById(req.params.id)
        voucher.state = true
        await voucher.save()
        if(!voucher) {
            return res.status(404).send()
        }
        res.send(voucher)
    } catch(e){
        res.status(400).send(e)
    }
})
//Delete voucher
router.patch('/vouchers/del/:id', async(req,res) => {
    try {
        //find voucher to edit by id
        const voucher = await Voucher.findById(req.params.id)
        voucher.isDeleted = true
        await voucher.save()
        if(!voucher) {
            return res.status(404).send()
        }
        res.send(voucher)
    }catch (e){
        res.status(400).send(e)
    }
})

module.exports = router