const Combo = require('../models/combo')
const Voucher = require('../models/voucher')
const ComboUser = require('../models/comboUser')
const VoucherUser = require('../models/voucherUser')
const moment = require('moment')
const Activity = require('../reuse/activity')
const Notification = require('../reuse/notification')

require('../db/mongoose')
const express = require('express')
const router = new express.Router();
moment().format()

router.post('/comboAPI/Promotions',async(req,res) => {
    try {
        const comboUser = await ComboUser.find({owner:req.body.user_name,to_date:{$gt:moment()}})
        let pickedVous =[]
        let filterVou = []
        if(comboUser.length!=0) {
            let i =0;
            for(const combo of comboUser) {
                filterVou.push((await combo.vouchers.filter(vou => vou.category===req.body.service && vou.count>=1)[0]))
                if(filterVou[i]) {
                    pickedVous.push({combo_id:combo.combo_id,combo_name:combo.combo_name,
                        value:filterVou[i].value, discount:filterVou[i].discount, service:filterVou[i].category })
                }
                i++
            }
        }
        const voucherUser = await VoucherUser.find({owner:req.body.user_name, service:req.body.service,
            isUsed:false,from_date:{$lt:moment()},to_date:{$gt:moment()}})
        if(pickedVous.length>0 || voucherUser.length>0) {
            return res.send({combos:pickedVous,vouchers:voucherUser})
        }
        res.status(404).send({error:"Not found!"})
    } catch (e) {
        res.status(500).send(e)
    }
})  
router.post('/comboAPI/PromotionCheck',async(req,res) => {
    //params: promotion_type, promotion_id, user_name, service, total
    const body = req.body
    if(!body.promotion_type || !body.promotion_id || !body.user_name || 
        !body.service || !body.total) {
            return res.status(400).send({Error:"following parameters must be enough: promotion_id, promotion_type, total, user_name, service"})
    }
    if(body.promotion_type != "combo" && body.promotion_type!="voucher") {
        return res.status(400).send({Error:"promotion_type is not in valid value: voucher || combo !!!"})
    }
    try {   
        let voucher = {} 
        let reduceMoney = 0
        let afterDiscount = 0
        if(body.promotion_type==="combo") {
            const comboUser = await ComboUser.findOne({combo_id:body.promotion_id,
                owner:body.user_name,to_date:{$gt:moment()}})
            if(comboUser) {
                const pickedVous = await comboUser.vouchers.filter(vou => (vou.category==body.service && vou.count>0))
                voucher = pickedVous[0]
            }
        } else if(body.promotion_type ==="voucher") {
            const voucherUser = await VoucherUser.findOne({voucher_id:body.promotion_id,owner:body.user_name,
                isUsed:false,from_date:{$lt:moment()},to_date:{$gt:moment()}})
            if(voucherUser) {
                voucher = voucherUser
            }
        }
        if(voucher) {
            if(voucher.discount!=0 && voucher.value!=0) {
                reduceMoney = ((body.total*voucher.discount/100) > voucher.value) ? voucher.value:(body.total*voucher.discount)/100
            } else {
                reduceMoney = voucher.value
            }
            afterDiscount = body.total - reduceMoney
            return res.status(200).send({afterDiscount, reduceMoney})
        }
        res.status(404).send({Error:"Promotion not found! || out of times to be used"})
    } catch (e) {
        res.status(500).send(e)
    }
})
router.post('/comboAPI/usingpromotion',async(req,res) => {
    const body = req.body
    if(!body.promotion_id || !body.promotion_type || !body.user_name || !body.service ) {
        return res.status(400).send({Error:"following properties must be enough: promotion_id, promotion type, user_name, service"});
    }
    if(body.promotion_type != "combo" && body.promotion_type!="voucher") {
        return res.status(400).send({Error:"promotion_type is not in valid value: voucher || combo !!!"})
    }
    try {
        //check if promotion is a combo or voucher
        if(body.promotion_type==="combo") { //if combo
            // flag1 to check if service in combo is available for using
            // flag2 to check whether combo is out of times to be used
            let flag1 = 0;
            let flag2 = 0
            const comboUser = await ComboUser.findOne({combo_id:body.promotion_id,owner:body.user_name,to_date:{$gt:moment()}})
            if(comboUser) {
                // loop each voucher in combo to check which voucher is the same with the using service
                for(const voucher of comboUser.vouchers) {
                    if(voucher.category == body.service && voucher.count>0) {
                        voucher.count-=1;
                        flag1 = 1
                        Activity.usingCombo(req.body.user_name,comboUser.combo_name,voucher.category)
                    }
                    if(voucher.count>=1) {
                        flag2 = 1
                    }
                }
            }
            // if flag1 = 1 means that user have used the combo successfully
            if(flag1==1) {
                // if flag2 == 0 means that there are no service in combo still be available for using
                // Log notificaton to user in order to inform that their combo is out of times to be used 
                if(flag2==0) {
                    Notification.outOfTimes(req.body.user_name,comboUser.combo_name)
                } 
                await comboUser.save()
                return res.status(200).send({status:"using combo successfully!!!"})
            }
        } else if(body.promotion_type ==="voucher") { //if voucher
            const countVoucher = await Voucher.findById(body.promotion_id)
            if(!countVoucher) {
                return res.status(404).send({status:"voucher not found!"})
            }
            if(countVoucher.used_times==countVoucher.times_to_use) {
                return res.status(404).send({status:"this voucher has reached to maximum used times"})
            }
            const voucherUser = await VoucherUser.findOne({voucher_id:body.promotion_id,owner:body.user_name,isUsed:false})
            if(voucherUser) {
                voucherUser.isUsed = true;
                await voucherUser.save()
                //Check if voucher is used to log activity of user
                if(voucherUser.isUsed) { 
                    Activity.usingVoucher(req.body.user_name,voucherUser.voucher_name)
                    countVoucher.used_times+=1
                    await countVoucher.save()
                }
                //return result when processing API successfully 
                return res.status(200).send({status:"using voucher successfully!!!"})
            }
        }
        res.status(404).send({status:"Promotion not found! || out of times to be used"})
    } catch (e) {
        res.status(500).send(e)
    }
})

module.exports = router         