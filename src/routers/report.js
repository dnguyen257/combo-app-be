const express = require('express')
const ComboUser = require('../models/comboUser')
const VoucherUser = require('../models/voucherUser')
const moment = require('moment')
const router = new express.Router();
require('../db/mongoose')
const { isObjectId } = require('../../utils/validate')

//---------------------- COMBO ---------------------------//
//--------------------------------------- sales report
// get number of all sold combos by combo id
// url: /report/combo/number-of-sold-combos/5dff38c05f160849c0bf1ea8
// return { "all": 20 }
router.get('/report/combo/number-of-sold-combos/:id', async (req, res) => {
    try {
        const comboId = req.params.id
        if (isObjectId(comboId) === false) throw new Error('comboID is not valid')
        const comboUsers = await ComboUser.find({ combo_id: comboId })
        res.send({ all: comboUsers.length })
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

// get daily sales report by comboId
/**
 * using params
 * comboId, start: 2019/10/01, end: 2019/10/01
 * 
 * return
 * {
    "records": [
        {
            "date": "30/10/2019",
            "timestamp": 1572368746440,
            "count": 0
        },
        {
            "date": "24/11/2019",
            "timestamp": 1574528746372,
            "count": 4
        },
        {
            "date": "24/12/2019",
            "timestamp": 1577120746394,
            "count": 13
        }
    ],
    "all": 20
}

// ko có combo nào được bán { records: [], all: 0 } 
 */
router.get('/report/combo/sales/daily', async (req, res) => {
    try {
        const comboId = req.query.comboId
        if (isObjectId(comboId) === false) throw new Error('comboID is not valid')
        let start = new Date(req.query.start)
        start.setHours(0, 0, 1)
        let end = new Date(req.query.end)
        end.setHours(23, 59, 59)
        const comboUsers = await ComboUser.find({
            combo_id: comboId,
            createdAt: { $gt: start.getTime(), $lt: end.getTime() }
        })
        if (comboUsers.length === 0) {
            res.send({ records: [], all: 0 })
        } else {
            const merged = comboUsers.reduce((acc, curr) => {
                const date = moment(curr.createdAt).format("DD/MM/YYYY")
                if (acc[date] === undefined) {
                    acc[date] = { date, timestamp: moment(curr.createdAt).valueOf(), count: 1 }
                } else {
                    acc[date] = { ...acc[date], count: acc[date].count + 1 }
                }
                return acc
            }, {})

            const records = Object.values(merged)
            records.sort((a, b) => a.timestamp - b.timestamp)
            res.send({ records: records, all: comboUsers.length })
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

// get monthly sales report by comboId
/**
 * using params
 * comboId,  start: 2019/10/01, end: 2019/12/1 //ngày nào trong tháng cũng được
 * 
 * return
 * {
    "records": [
        {
            "date": "10/2019",
            "timestamp": 1572368746440,
            "count": 1
        },
        {
            "date": "11/2019",
            "timestamp": 1574528746372,
            "count": 5
        },
        {
            "date": "12/2019",
            "timestamp": 1577120746394,
            "count": 14
        }
    ],
    "all": 20
}
// ko có combo nào được bán { records: [], all: 0 } 
 */
router.get('/report/combo/sales/monthly', async (req, res) => {
    try {
        const comboId = req.query.comboId
        if (isObjectId(comboId) === false) throw new Error('comboID is not valid')
        let start = new Date(req.query.start)
        start.setDate(1)
        start.setHours(0, 0, 0)
        let end = moment(req.query.end).add(1, 'months').toDate()
        end.setDate(1)
        end.setHours(0, 0, 0)
        const comboUsers = await ComboUser.find({
            combo_id: comboId,
            createdAt: { $gt: start.getTime(), $lt: end.getTime() }
        })
        if (comboUsers.length === 0) {
            res.send({ records: [], all: 0 })
        } else {
            const merged = comboUsers.reduce((acc, curr) => {
                const date = moment(curr.createdAt).format("MM/YYYY")
                if (acc[date] === undefined) {
                    acc[date] = { date, timestamp: moment(curr.createdAt).valueOf(), count: 1 }
                } else {
                    acc[date] = { ...acc[date], count: acc[date].count + 1 }
                }
                return acc
            }, {})

            const records = Object.values(merged)
            records.sort((a, b) => a.timestamp - b.timestamp)
            res.send({ records: records, all: comboUsers.length })
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

// get yearly sales report by comboId
/**
 * using params
 * comboId,  start: 2019, end: 2020 
 * 
 * return
 * {
    "records": [
        {
            "date": "2018",
            "timestamp": 1545584746501,
            "count": 1
        },
        {
            "date": "2019",
            "timestamp": 1574528746372,
            "count": 19
        }
    ],
    "all": 20
}
// ko có combo nào được bán { records: [], all: 0 } 
 */
router.get('/report/combo/sales/yearly', async (req, res) => {
    try {
        const comboId = req.query.comboId
        if (isObjectId(comboId) === false) throw new Error('comboID is not valid')
        const start = new Date(req.query.start, 0, 1, 0, 1, 1)
        const end = new Date(req.query.end, 11, 31, 23, 59, 59)
        const comboUsers = await ComboUser.find({
            combo_id: comboId,
            createdAt: { $gt: start.getTime(), $lt: end.getTime() }
        })
        if (comboUsers.length === 0) {
            res.send({ records: [], all: 0 })
        } else {
            const merged = comboUsers.reduce((acc, curr) => {
                const date = moment(curr.createdAt).format("YYYY")
                if (acc[date] === undefined) {
                    acc[date] = { date, timestamp: moment(curr.createdAt).valueOf(), count: 1 }
                } else {
                    acc[date] = { ...acc[date], count: acc[date].count + 1 }
                }
                return acc
            }, {})

            const records = Object.values(merged)
            records.sort((a, b) => a.timestamp - b.timestamp)
            res.send({ records: records, all: comboUsers.length })
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

//--------------------------------------- revenue report
// get daily revenue report by comboId
/**
 * using params
 * comboId, start: 2019/10/01, end: 2019/10/01
 * 
 * return
 * {
    "records": [
        {
            "date": "30/10/2019",
            "timestamp": 1572368746440,
            "total": 320000,
            "count": 1
        },
        {
            "date": "24/11/2019",
            "timestamp": 1574528746372,
            "total": 1600000,
            "count": 5
        },
        {
            "date": "24/12/2019",
            "timestamp": 1577120746394,
            "total": 4160000,
            "count": 13
        }
    ],
    "all": 19,
    "total": 6080000
}

// ko có combo nào được bán { records: [], all: 0, total: 0 } 
 */
router.get('/report/combo/revenue/daily', async (req, res) => {
    try {
        const comboId = req.query.comboId
        if (isObjectId(comboId) === false) throw new Error('comboID is not valid')
        let start = new Date(req.query.start)
        start.setHours(0, 0, 1)
        let end = new Date(req.query.end)
        end.setHours(23, 59, 59)
        const comboUsers = await ComboUser.find({
            combo_id: comboId,
            createdAt: { $gt: start.getTime(), $lt: end.getTime() }
        })
        if (comboUsers.length === 0) {
            res.send({ records: [], all: 0, total: 0 })
        } else {
            let total = 0
            const merged = comboUsers.reduce((acc, curr) => {
                total = total + curr.value
                const date = moment(curr.createdAt).format("DD/MM/YYYY")
                if (acc[date] === undefined) {
                    acc[date] = { date, timestamp: moment(curr.createdAt).valueOf(), total: curr.value, count: 1 }
                } else {
                    acc[date] = { ...acc[date], total: acc[date].total + curr.value, count: acc[date].count + 1 }
                }
                return acc
            }, {})

            const records = Object.values(merged)
            records.sort((a, b) => a.timestamp - b.timestamp)
            res.send({ records: records, all: comboUsers.length, total: total })
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

// get monthly revenue report by comboId
/**
 * using params
 * comboId,  start: 2019/10/01, end: 2019/12/1 //ngày nào trong tháng cũng được
 * 
 * return
 * {
    "records": [
        {
            "date": "10/2019",
            "timestamp": 1572368746440,
            "count": 1,
            "total": 320000
        },
        {
            "date": "11/2019",
            "timestamp": 1574528746372,
            "count": 5,
            "total": 1600000
        },
        {
            "date": "12/2019",
            "timestamp": 1577120746394,
            "count": 13,
            "total": 4160000
        }
    ],
    "all": 19,
    "total": 6080000
}
// ko có combo nào được bán { records: [], all: 0, total: 0 } 
 */
router.get('/report/combo/revenue/monthly', async (req, res) => {
    try {
        const comboId = req.query.comboId
        if (isObjectId(comboId) === false) throw new Error('comboID is not valid')
        let start = new Date(req.query.start)
        start.setDate(1)
        start.setHours(0, 0, 0)
        let end = moment(req.query.end).add(1, 'months').toDate()
        end.setDate(1)
        end.setHours(0, 0, 0)
        const comboUsers = await ComboUser.find({
            combo_id: comboId,
            createdAt: { $gt: start.getTime(), $lt: end.getTime() }
        })
        if (comboUsers.length === 0) {
            res.send({ records: [], all: 0, total: 0 })
        } else {
            let total = 0
            const merged = comboUsers.reduce((acc, curr) => {
                total = total + curr.value
                const date = moment(curr.createdAt).format("MM/YYYY")
                if (acc[date] === undefined) {
                    acc[date] = { date, timestamp: moment(curr.createdAt).valueOf(), count: 1, total: curr.value }
                } else {
                    acc[date] = { ...acc[date], count: acc[date].count + 1, total: acc[date].total + curr.value }
                }
                return acc
            }, {})
            const records = Object.values(merged)
            records.sort((a, b) => a.timestamp - b.timestamp)
            res.send({ records: records, all: comboUsers.length, total })
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

// get yearly revenue report by comboId
/**
 * using params
 * comboId,  start: 2019, end: 2020 
 * 
 * return
 * {
    "records": [
        {
            "date": "2018",
            "timestamp": 1545584746501,
            "count": 1,
            "total": 320000
        },
        {
            "date": "2019",
            "timestamp": 1574528746372,
            "count": 19,
            "total": 6080000
        }
    ],
    "all": 20,
    "total": 6400000
}
// ko có combo nào được bán { records: [], all: 0, total: 0 } 
 */
router.get('/report/combo/revenue/yearly', async (req, res) => {
    try {
        const comboId = req.query.comboId
        if (isObjectId(comboId) === false) throw new Error('comboID is not valid')
        const start = new Date(req.query.start, 0, 1, 0, 1, 1)
        const end = new Date(req.query.end, 11, 31, 23, 59, 59)
        const comboUsers = await ComboUser.find({
            combo_id: comboId,
            createdAt: { $gt: start.getTime(), $lt: end.getTime() }
        })
        if (comboUsers.length === 0) {
            res.send({ records: [], all: 0 })
        } else {
            let total = 0
            const merged = comboUsers.reduce((acc, curr) => {
                total = total + curr.value
                const date = moment(curr.createdAt).format("YYYY")
                if (acc[date] === undefined) {
                    acc[date] = { date, timestamp: moment(curr.createdAt).valueOf(), count: 1, total: curr.value }
                } else {
                    acc[date] = { ...acc[date], count: acc[date].count + 1, total: acc[date].total + curr.value }
                }
                return acc
            }, {})

            const records = Object.values(merged)
            records.sort((a, b) => a.timestamp - b.timestamp)
            res.send({ records: records, all: comboUsers.length, total })
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

//---------------------- GIFT VOUCHER ---------------------------// BUY VOUCHER is unavailable
//----------------------number of all used voucher report
// get number of all used voucher by voucherId
// url: /report/voucher/number-of-used-vouchers/5dff38c05f160849c0bf1ea8
// return { "all": 20 }
router.get('/report/voucher/number-of-used-vouchers/all/:id', async (req, res) => {
    try {
        const voucherId = req.params.id
        if (isObjectId(voucherId) === false) throw new Error('voucherID is not valid')
        const voucherUsers = await VoucherUser.find({ voucher_id: voucherId, isUsed: true })
        res.send({ all: voucherUsers.length })
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

// get daily  report by voucherId
/**
 * using params
 * voucherId, start: 2019/10/01, end: 2019/10/01
 * 
 * return
 * {
    "records": [
        {
            "date": "01/01/2019",
            "timestamp": 1546312945853,
            "count": 2
        },
        {
            "date": "24/05/2019",
            "timestamp": 1558668145852,
            "count": 3
        }
    ],
    "all": 5
}
// ko có voucher nao duoc su dung { records: [], all: 0 } 
 */
router.get('/report/voucher/number-of-used-vouchers/daily', async (req, res) => {
    try {
        const voucherId = req.query.voucherId
        if (isObjectId(voucherId) === false) throw new Error('voucherID is not valid')
        let start = new Date(req.query.start)
        start.setHours(0, 0, 1)
        let end = new Date(req.query.end)
        end.setHours(23, 59, 59)
        const voucherUsers = await VoucherUser.find({
            voucher_id: voucherId,
            isUsed: true,
            createdAt: { $gt: start.getTime(), $lt: end.getTime() }
        })
        if (voucherUsers.length === 0) {
            res.send({ records: [], all: 0 })
        } else {
            const merged = voucherUsers.reduce((acc, curr) => {
                const date = moment(curr.createdAt).format("DD/MM/YYYY")
                if (acc[date] === undefined) {
                    acc[date] = { date, timestamp: moment(curr.createdAt).valueOf(), count: 1 }
                } else {
                    acc[date] = { ...acc[date], count: acc[date].count + 1 }
                }
                return acc
            }, {})

            const records = Object.values(merged)
            records.sort((a, b) => a.timestamp - b.timestamp)
            res.send({ records: records, all: voucherUsers.length })
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

// get monthly report by voucherId
/**
 * using params
 * voucherId,  start: 2019/10/01, end: 2019/12/1 //ngày nào trong tháng cũng được
 * 
 * return
 * {
    "records": [
        {
            "date": "10/2019",
            "timestamp": 1572368746440,
            "count": 1
        },
        {
            "date": "11/2019",
            "timestamp": 1574528746372,
            "count": 5
        },
        {
            "date": "12/2019",
            "timestamp": 1577120746394,
            "count": 14
        }
    ],
    "all": 20
}
// ko có voucher nao duoc su dung { records: [], all: 0 } 
 */
router.get('/report/voucher/number-of-used-vouchers/monthly', async (req, res) => {
    try {
        const voucherId = req.query.voucherId
        if (isObjectId(voucherId) === false) throw new Error('voucherID is not valid')
        let start = new Date(req.query.start)
        start.setDate(1)
        start.setHours(0, 0, 0)
        let end = moment(req.query.end).add(1, 'months').toDate()
        end.setDate(1)
        end.setHours(0, 0, 0)
        const voucherUsers = await VoucherUser.find({
            voucher_id: voucherId,
            isUsed: true,
            createdAt: { $gt: start.getTime(), $lt: end.getTime() }
        })
        if (voucherUsers.length === 0) {
            res.send({ records: [], all: 0 })
        } else {
            const merged = voucherUsers.reduce((acc, curr) => {
                const date = moment(curr.createdAt).format("MM/YYYY")
                if (acc[date] === undefined) {
                    acc[date] = { date, timestamp: moment(curr.createdAt).valueOf(), count: 1 }
                } else {
                    acc[date] = { ...acc[date], count: acc[date].count + 1 }
                }
                return acc
            }, {})

            const records = Object.values(merged)
            records.sort((a, b) => a.timestamp - b.timestamp)
            res.send({ records: records, all: voucherUsers.length })
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

// get yearly report by voucherId
/**
 * using params
 * voucherId,  start: 2019, end: 2020 
 * 
 * return
 * {
    "records": [
        {
            "date": "2018",
            "timestamp": 1545584746501,
            "count": 1
        },
        {
            "date": "2019",
            "timestamp": 1574528746372,
            "count": 19
        }
    ],
    "all": 20
}
// ko có voucher nao duoc su dung { records: [], all: 0 } 
 */
router.get('/report/voucher/number-of-used-vouchers/yearly', async (req, res) => {
    try {
        const voucherId = req.query.voucherId
        if (isObjectId(voucherId) === false) throw new Error('voucherID is not valid')
        const start = new Date(req.query.start, 0, 1, 0, 1, 1)
        const end = new Date(req.query.end, 11, 31, 23, 59, 59)
        const voucherUsers = await VoucherUser.find({
            voucher_id: voucherId,
            isUsed: true,
            createdAt: { $gt: start.getTime(), $lt: end.getTime() }
        })
        if (voucherUsers.length === 0) {
            res.send({ records: [], all: 0 })
        } else {
            const merged = voucherUsers.reduce((acc, curr) => {
                const date = moment(curr.createdAt).format("YYYY")
                if (acc[date] === undefined) {
                    acc[date] = { date, timestamp: moment(curr.createdAt).valueOf(), count: 1 }
                } else {
                    acc[date] = { ...acc[date], count: acc[date].count + 1 }
                }
                return acc
            }, {})

            const records = Object.values(merged)
            records.sort((a, b) => a.timestamp - b.timestamp)
            res.send({ records: records, all: voucherUsers.length })
        }
    } catch (error) {
        res.status(400).send({ message: error.message })
    }
})

module.exports = router

//c:/Users/admin/mongodb/bin/mongod.exe --dbpath=c:/Users/admin/mongodb-data
