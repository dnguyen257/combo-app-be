const Combo = require('../models/combo')
const Voucher = require('../models/voucher')
const ComboUser = require('../models/comboUser')
const VoucherUser = require('../models/voucherUser')
const ComboConfig = require('../models/comboConfig')
const ActivityModel = require('../models/activity')
const NotificationModel = require('../models/notification')
var mongoose = require('mongoose');

const moment = require('moment')
moment().format()

const Payment = require('../../utils/payment')
const Notification = require('../reuse/notification')
const Activity = require('../reuse/activity')
const express = require('express')
const router = new express.Router();

require('../db/mongoose')



//get combo of user by id
router.get('/passenger/combobyid',async (req,res)=> {
    try {
        const combo = await ComboUser.findOne({owner:req.query.user_name,combo_id:req.query.combo_id,to_date:{$gt:moment()}})
        if(!combo) {
            return res.status(404).send({Error: "Combo does not exist"})
        }
        res.send(combo)
    } catch(e) {
        res.status(500).send(e)
    }
})
router.get('/passenger/myactivity',async (req,res)=> {
    try {
        const activity = await ActivityModel.find({owner:req.query.user_name})
        if(activity.length==0) {
            return res.status(404).send({message: "there are no activities"})
        }
        res.send(activity)
    } catch(e) {
        res.status(500).send(e)
    }
})
router.get('/passenger/mynoti',async (req,res)=> {
    try {
        const notification = await NotificationModel.find({owner:req.query.user_name})
        if(notification.length==0) {
            return res.status(404).send({Error: "there are no notifications"})
        }
        res.send(notification)
    } catch(e) {
        res.status(500).send(e)
    }
})
//get combos that user already registered
router.get('/passenger/mycombo',async (req,res)=> {
    try {
        const combos = await ComboUser.find({owner: req.query.user_name,to_date:{$gt:moment()}})
        const comboConfigs = await ComboConfig.find({user_name: req.query.user_name})
        if(combos.length==0) {
            return res.status(404).send({Error: "There are no combos for this user"})
        }
        res.send({combos,comboConfigs})
    } catch(e) {
        res.status(500).send(e)
    }
})
//get vouchers that user already registered
router.get('/passenger/myvoucher',async (req,res)=> {
    try {
        const vouchers = await VoucherUser.find({owner: req.query.user_name, isUsed:false,
            from_date:{$lt:moment()},to_date:{$gt:moment()}})
        if(vouchers.length==0) {
            return res.status(404).send({Error: "There are no vouchers for this user"})
        }
        res.send(vouchers)
    } catch(e) {
        res.status(500).send(e) 
    }
})
// auto renew combo for user
router.post('/passenger/autorenew',async(req,res) => {
    try {
        const config = await ComboConfig.findOne({combo_id:req.body.combo_id, user_name:req.body.user_name})
        if(!config) {
            return res.status(404).send({error:"can not configure, combo not found!"})
        }
        if(config.autoRenew == true) {
            return res.status(400).send({message: "You have already automated it!"})
        }
        config.autoRenew=true
        await config.save()
        res.status(200).send({message: "Automated combo renew successfully!"})
    } catch (e) {
        res.status(500).send(e)
    }
})
//stop renew combo for user
router.post('/passenger/stoprenew',async(req,res) => {
    try {
        const config = await ComboConfig.findOne({combo_id:req.body.combo_id, user_name:req.body.user_name})
        if(!config) {
            return res.status(404).send({error:"can not configure, combo not found!"})
        }
        if(config.autoRenew ==false) {
            return res.status(400).send({message: "You have already stopped it!"})
        }
        config.autoRenew=false
        await config.save()
        res.status(200).send({message: "Stopped combo renew successfully!"})
    } catch (e) {
        res.status(500).send(e)
    }
})
//register a new combo for user
router.post('/combos/register',async(req,res) => { 
    try {
        const combo = await Combo.findById(req.body.combo_id)
        if(combo) {
            let combouser = await ComboUser.findOne({combo_id:combo.id,owner:req.body.user_name,to_date:{$gt:moment()}})
            let action = "register";
            if(!combouser) {
                //generate order_id
                const objectid = new mongoose.Types.ObjectId()
                // navigate to payment api to minus money from general wallet, add money to membership wallet
                const paymentRes = await Payment.addToWallet(req.body.user_name,combo.value,objectid)
                if(paymentRes === 400) {
                    Activity.buyCombo(req.body.user_name,combo.combo_name,action==="renew"?"gia hạn thất bại":"đăng ký thất bại")
                    throw new Error("Registered fail")
                } else {
                    Activity.buyCombo(req.body.user_name,combo.combo_name,action==="renew"?"gia hạn":"đăng ký")
                    Notification.actionNoti(req.body.user_name,combo.combo_name,action==="renew"?"gia hạn":"đăng ký")
                }
                //find comboUser if it is existed
                combouser = await ComboUser.find({combo_id:combo.id,owner:req.body.user_name})
                //check if user renew or register combo
                if(combouser.length>=1) { //if renew
                    action = "renew"
                } else {                  //if register
                    const config = new ComboConfig({user_name:req.body.user_name, combo_id:req.body.combo_id})
                    await config.save()
                }
                
                const newcombouser = new ComboUser({_id:objectid,combo_id:combo.id, combo_name:combo.combo_name, action:action,
                    owner: req.body.user_name,value:combo.value,vouchers:combo.voucher_array,
                    to_date:moment().add(combo.days,"d")})
                await newcombouser.save()
                //create activity, notification after registering, renewing combo for user
                return res.status(201).send(newcombouser)
                
            }
            return res.status(400).send({Error: "You have already registered this combo for user"})
        }
        res.status(404).send({Error: "Combo does not exist"})
    } catch(e) {
        res.status(500).send({e})
    }
})
//register a new voucher for user
router.post('/vouchers/register',async(req,res) => {
    try {
        const voucher = await Voucher.findById(req.body.voucher_id)
        if(voucher) {
            const voucheruser = new VoucherUser({voucher_id:voucher.id, voucher_name:voucher.voucher_name,
                 owner: req.body.user_name,service:voucher.subcategory, value:voucher.value, discount: voucher.discount,
                 from_date:moment(),to_date:moment().add(30,"d")})
            await voucheruser.save()
            return res.status(201).send(voucheruser)
        }
        res.status(404).send({Error: "voucher does not exist"})   
    } catch(e) {
        res.status(500).send(e)
    }
})
module.exports = router