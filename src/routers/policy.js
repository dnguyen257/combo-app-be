const express = require('express')
const Policy = require('../models/policy')
const router = new express.Router();
const auth = require('../middleware/auth')
require('../db/mongoose')
//List policies
router.get('/policies', async (req,res)=> {
    try{ 
        const policies = await Policy.find({})
        if(policies.length==0) {
            return res.status(404).send()
        }
        res.send(policies)
    } catch(e) {
        res.status(500).send(e)
    }
})
//Seach by id
router.get('/policies/:id',async (req,res)=> {
    const _id = req.params.id
    try {
        const policy = await Policy.findById(_id)
        if(!policy) {
            return res.status(404).send()
        }
        res.send(policy)
    } catch(e) {
        res.status(500).send(e)
    }
})

//create object
router.post('/policies/new',async (req,res) => {
    // if (req.body.category =="voucher" && req.body.voucher_percent) {
    //     return res.status(400).send({error: 'Invalid policy added! voucher policy can not include voucher_percent'})
    // }
    const policy = new Policy(req.body)
    try{ 
        await policy.save()
        res.status(201).send(policy)
    } catch(e) {
        res.status(400).send(e)
    }
})

//Edit object
router.patch('/policies/edit/:id', async(req,res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['policy_name','description','voucher_percent','extra_percent']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))
    if(!isValidOperation) {
        return res.status(400).send({error: "Invalid policy updates!"})
    }
    try {
        const policy = await Policy.findById(req.params.id)
        updates.forEach((update) => policy[update] = req.body[update])
        await policy.save()
        if(!policy) {
            return res.status(404).send()
        }
        res.send(policy)
    }catch(e){
        res.status(400).send(e)
    }
})

//delete objects
router.patch('/policies/del/:id', async(req,res) => {
    const _id = req.params.id
    try {
        const policy = await Policy.findById(req.params.id)
        policy.isDeleted = true
        await policy.save()
        if(!policy) {
            return res.status(404).send()
        }
        res.send(policy)
    }catch (e){
        res.status(400).send(e)
    }
})

module.exports = router