const Activity = require('../models/activity')
const moment = require('moment')
require('../db/mongoose')
moment().format()

// create an activity when using voucher
const usingVoucher = (owner,target) => {
    try {
        content = "Bạn đã sử dụng voucher: "+target+" lúc " + moment().format('lll').toString()
        const activity = new Activity({owner:owner,content:content})
        activity.save()
    } catch(e) {
        throw new Error("Invalid activity: "+e)
    }
}
// create an activity when using combo
const usingCombo = (owner,target,service) => {
    try {
        const content = "Bạn đã sử dụng combo: "+target+"( dịch vụ: "+service+" ) lúc " + moment().format('lll').toString()
        const activity = new Activity({owner:owner,content:content})
        activity.save()
    } catch(e) {    
        throw new Error("Invalid activity: "+e)
    }
}

const buyCombo = (owner,target,action) => {
    try {
        const content = "Bạn đã "+action+" combo: "+target+" lúc " + moment().format('lll').toString()
        const activity = new Activity({owner:owner,content:content})
        activity.save()
    } catch(e) {    
        throw new Error("Invalid activity: "+e)
    }
}

module.exports = {usingVoucher,usingCombo,buyCombo}