//framework
const express = require('express')
var cors = require('cors')

//Routers
const userRouter = require('./routers/user')
const comboRouter = require('./routers/combo')
const voucherRouter = require('./routers/voucher')
const campaignRouter = require('./routers/campaign')
const policyRouter = require('./routers/policy')
const apiRouter = require('./routers/comboAPI')
const passengerRouter = require('./routers/passenger')
const reportRouter = require('./routers/report')

//Set environment variables
const app = express();
const port = process.env.PORT

//Config framework
app.use(express.urlencoded({extended:false}))
app.use(express.json())
app.use(cors())

//Initial API
app.get('',(req,res) => {
    res.send("Welcome to Combo application API !!")
})
//Ping route
app.get('ping',(req,res) => {
    res.json({
        msg: 'PONG',
        server: 'waza-membership',
        uptime: process.uptime(),
    })
})
//Using routers
app.use(userRouter)
app.use(comboRouter)
app.use(voucherRouter)
app.use(campaignRouter)
app.use(policyRouter)
app.use(apiRouter)
app.use(passengerRouter)
app.use(reportRouter)

//Confirm that server is running
app.listen(port,()=> {
    console.log('Server is running on port '+ port);
})

module.exports = app