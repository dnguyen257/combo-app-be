const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const voucherUserSchema = new Schema({
    owner: {
        type: String,
        required: true,
    },
    voucher_id : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'voucher',
        required: true,
    },
    voucher_name: {
        type: String,
        required: true,
    },
    from_date : {
        type:Date,
        required: true,
    },
    to_date: {
        type:Date,
        required: true,
    },
    discount: {
        type:String,
        default:30
    },
    value:{
        type:Number,
        default: 30000
    },
    service: {
        type:String,
        enum:['food','move','shopping','bike']
    },
    isUsed: {
        type:Boolean,
        default:false
    }
}, {
    timestamps: true
}) 

const VoucherUser = mongoose.model('VoucherUser',voucherUserSchema)

module.exports = VoucherUser