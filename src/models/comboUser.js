const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const comboUserSchema = new Schema({
    owner: {
        type:String,
        required:true
    },
    combo_id : {
        type: Schema.Types.ObjectId,
        ref:"combo",
        required:true,
    },
    combo_name: {
        type: String
    },
    vouchers: [{
        voucher_id:{
            type: mongoose.Schema.Types.ObjectId,
            required:true,
            ref: 'voucher'
        },
        voucher_name:{
            type:String,
            required:true,
        },
        category: {
            type: String,
            required:true,
            enum:['food','move','shopping','bike']
        },
        discount:{
            type:Number,
            default:0,
            max:100,
            min:0
        },
        value:{
            type:Number,
            default:0,
            required:true
        },
        count: {
            type: Number,
            default: 1,
            max:50,
        }
    }],
    to_date: {
        type: Date,
        required:true,
    },
    status: {    
        type: String,
        required:true,
        default: "current",
        enum: ["current","out of date"]
    },
    action: {
        type: String,
        enum:["register","renew"]
    },
    value: {
        type: Number,
        required: true,
    }
}, {
    timestamps: true
})

const ComboUser = mongoose.model('ComboUser',comboUserSchema)

module.exports = ComboUser