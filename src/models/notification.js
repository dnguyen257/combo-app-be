const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const notificationSchema = new Schema({
    owner: {
        type:Schema.Types.ObjectId,
        required:true
    },
    content: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

const Notification = mongoose.model('notification',notificationSchema)

module.exports = Notification