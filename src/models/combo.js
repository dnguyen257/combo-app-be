const mongoose = require('mongoose')

const moment = require('moment')

const Schema = mongoose.Schema;

const ComboSchema = new Schema({
    combo_name: {
        type: String, 
        required:true,
        unique: true,
        maxlength: 50,
    },
    description: {
        type: String,
        maxlength:1000
    },
    value: {
        type: Number,
        required: [true,'value cannot empty']
    },
    policy_id: {
        type: Schema.Types.ObjectId,
        ref:'policy'
    },
    state: {
        type: Boolean,
        default: true,
    }, 
    from_date: {
        type: Date,
        required: true,
    },
    to_date: {
        type: Date,
        default:"12/30/2999"
    },
    voucher_array:[{
        voucher_id:{
            type: mongoose.Schema.Types.ObjectId,
            required:true,
            ref: 'voucher'
        },
        voucher_name:{
            type:String,
            required:true,
        },
        category: {
            type: String,
            required:true,
            enum:['food','move','shopping','bike']
        },
        discount:{
            type:Number,
            max:100,
            min:0
        },
        value:{
            type:Number,
            required:true
        },
        count: {
            type: Number,
            default: 1,
            max:50,
        }
    }],
    isDeleted: {
        type: Boolean,
        default: false,
    },
    days:{
        type: Number,
        required: true,
        defaul: 30,
        validate(value) {
            if(value<0) {
                throw new Error('days must be a positive number')
            }
        },
        min: 30,
    },
}, {
    timestamps: true
});
const Combo = mongoose.model('combo',ComboSchema)
module.exports = Combo  