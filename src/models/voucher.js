const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const VoucherSchema = new Schema({
    voucher_name: {
        type: String,
        required: true,
        unique: true,
        maxlength: 50,
    },
    description: {
        type: String,
        maxlength:1000
    },
    discount: {
        type: Number,
        default: 0,
    },
    value: {
        type: Number,
        default: 0
    },
    rank: {
        type: Number,
        default:0,
    },
    state: {
        type: Boolean,
        default: true,
    },
    category: {
        type: String,
        enum: ['gift','buy']
    },
    subcategory: {
        type: String,
        enum: ['food','move','shopping','bike']
    },
    used_times: {
        type:Number,
        default: 0
    },
    times_to_use: { 
        type:Number,
        required: [true,'times_to_use need to be supplied']
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
}, {
    timestamps: true
})
const Voucher = mongoose.model('voucher',VoucherSchema)

module.exports = Voucher