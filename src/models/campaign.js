const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const CampaignSchema = new Schema({
    campaign_name: {
        type: String,
        required: true,
        unique: true,
    },
    from_date : {
        type:Date,
        required: true
    },
    to_date: {
        type:Date,
        required: true
    },
    description: {
        type: String,
    },
    vouchers: {
        type:[{type:Schema.Types.ObjectId}],
    },
    isDeleted: {
        type: Boolean,
        default: false,
    }
}, {
    timestamps: true
})
const Campaign = mongoose.model('campaign',CampaignSchema)

module.exports = Campaign