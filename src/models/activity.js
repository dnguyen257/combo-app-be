const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const activitySchema = new Schema({
    owner: {
        type:Schema.Types.ObjectId,
        required:true
    },
    content: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

const Activity = mongoose.model('activities',activitySchema)

module.exports = Activity