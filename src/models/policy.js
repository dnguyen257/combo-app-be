const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const policySchema = new Schema({
    policy_name: {
        type: String,
        required: true,
        unique: true,
    },
    description: {
        type: String,
    },
    extra_percent: {
        type:Number,
        default:30,
        min:5,
        max:100
    },
    voucher_percent:{
        type:[{type:Number,
            max:100,
            min:5
        }],
        required:true,
        validate(value) {
            if(value && value.length!=0) {
                if(value.length>4) {
                    throw new Error('Voucher percent array length must be lower or equal to 4 ')
                }
                let total = 0
                for(const x of value) {
                    total += x;
                }
                if(total!=100) {
                    throw new Error('Total value of array must be 100')
                }
            }
       }
    },
    isDeleted: {
        type: Boolean,
        default: false,
    }
}, {
    timestamps: true
})

const Policy = mongoose.model('policy',policySchema)

module.exports = Policy