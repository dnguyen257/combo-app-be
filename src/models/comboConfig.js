const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const ComboConfigSchema = new Schema({
    user_name: {
        type: Schema.Types.ObjectId,
        ref:"UserInfo",
        required:true,
    },
    combo_id : {
        type: Schema.Types.ObjectId,
        ref:"combo",
        required:true,
    },
    autoRenew: {
        type: Boolean,
        default: true,
    }
}, {    timestamps: true
})

const ComboConfig = mongoose.model('comboconfig',ComboConfigSchema)

module.exports = ComboConfig