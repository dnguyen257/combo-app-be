const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const userSchema = mongoose.Schema({
    username:{
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
        trim: true,
        minlength: 6,
        validate(value) {
            if(!value.match("(?=.*[a-z])")) {
                throw new Error('password must contain at least 1 lowercase character')
            }
            if(!value.match("(?=.*[A-Z])")) {
                throw new Error('password must contain at least 1 uppercase character')
            }
            if(!value.match("(?=.*[0-9])")) {
                throw new Error('password must contain at least 1 numeric character')
            }
            if(!value.match("(?=.[!@#\$%\^&])")) {
                throw new Error('password must contain at least 1 special character')
            }
        }
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
    tokens: [{
        token: {
            type: String,
            required: true,
        }
    }]
}, {
    timestamps: true
})

userSchema.methods.generateAuthToken = async function () {
    const user = this 
    const token =jwt.sign({_id: user._id.toString()}, process.env.JWT_SECRET)
    if(!token) {
        throw new Error('Unable to generate token')
    }
    user.tokens = user.tokens.concat({token})
    await user.save()
    return token
}

userSchema.methods.toJSON = function() {
    const user = this
    const userObject = user.toObject()

    delete userObject.password
    delete userObject.tokens
    
    return userObject
}

userSchema.statics.findByCredentials = async(username,password) => {
    const user = await User.findOne({username})
    if(!user) {
        throw new Error('Unable to login')
    }
    const isMatch = await bcrypt.compare(password,user.password)

    if(!isMatch) {
        throw new Error('Unable to login')
    }
    return user
}

userSchema.pre('save',async function (next) {
    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password,8)
    }
    next()
})
const User = mongoose.model('user',userSchema)
module.exports = User