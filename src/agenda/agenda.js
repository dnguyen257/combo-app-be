require('../db/mongoose')
const agenda = require('agenda')
const ComboUser = require('../models/comboUser')
const Combo = require('../models/combo')
const ComboConfig = require('../models/comboConfig')
const moment = require('moment')

moment().format()
agenda.define('renew combo', async job => {
    let combouser = {}
    let combo = {}
    const comboconfig = await ComboConfig.find({autoRenew:true})
    comboconfig.forEach(async (config) => {
        combouser = await ComboUser.findOne({id:config.combo_id,to_date:{$gt: moment()}})
        if(!combouser) {
            combo = await Combo.findById(config.combo_id)
            ComboUser.create([{combo_id:combo.id, }])
        }
    })

});

